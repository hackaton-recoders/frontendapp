import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { UserModel } from 'src/app/models/UserModel';
import Swal from 'sweetalert2';
import { UsersService } from '../../services/users.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  public user: UserModel = new UserModel();

  constructor(
    private router: Router,
    private usuarioService: UsersService) { }

  ngOnInit() {}

  login( form: NgForm ) {
    
    if (  form.invalid ) { return; }

    Swal.fire({  allowOutsideClick: false, icon: 'info', text: 'Espera por Favor..'});
    Swal.showLoading();


    this.usuarioService.signIn(this.user)
      .subscribe( resp =>{

      Swal.close();
      

      this.router.navigateByUrl('/home');

    },(erro)=>{
      
      Swal.fire({   icon: 'error', text: erro.error.title, title:'Error al Iniciar Sesión'});
 
    });


  }

  goToRegister(){
    this.router.navigateByUrl('/register');
  }

}
