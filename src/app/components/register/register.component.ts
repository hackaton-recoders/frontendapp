import { Component, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';

import { UserModel } from 'src/app/models/UserModel';
import { UsersService } from '../../services/users.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: UserModel = new UserModel();

  constructor(
    private router: Router,
    private usuarioService: UsersService
  ) { }

  ngOnInit() {
  }

  register(form: NgForm){
    if (  form.invalid ) { return; }

    Swal.fire({  allowOutsideClick: false, icon: 'info', text: 'Espera por Favor..'});
    Swal.showLoading();


    this.usuarioService.signUP(this.user)
      .subscribe( resp =>{

      Swal.close();
      

      this.router.navigateByUrl('/home');

    },(erro)=>{
      
      Swal.fire({   icon: 'error', text: erro.error.title, title:'Error al registrarse'});
 
    });


  } 

}