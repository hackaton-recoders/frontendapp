import { User } from "../interfaces/user";

export class UserModel implements User{
    nombre: string;
    correo: string;
    password: string;
    apellidos?: string;
    telefono?: string;

}