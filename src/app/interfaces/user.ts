export interface User {
  nombre: string,
  correo: string,
  password: string,
  apellidos?: string,
  telefono?: string,
}

export interface UserLogin{
  correo: string,
  password: string,
}
