/**ROUTES */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**COMPONENTS */
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent} from './components/register/register.component';


/**ROUTES */
const routes: Routes = [
  { path: "login", component: LoginComponent},
  { path: "home",  component: HomeComponent},
  { path: 'register', component: RegisterComponent},
  { path: "**", pathMatch:"full", redirectTo: "home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
