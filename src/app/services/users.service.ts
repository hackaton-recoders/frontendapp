import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

import { User, UserLogin } from '../interfaces/user';


@Injectable({
  providedIn: 'root'
})

export class UsersService {

  private endpoint = environment.API_URL; 

  constructor(private http: HttpClient) { }

  signUP(user:User): Observable<any>{
    return this.http.post(`${this.endpoint}/api/v1/signup`, user);
  }

  signIn(user:UserLogin): Observable<any>{
    return this.http.post(`${this.endpoint}/api/v1/signin`, user);
  }

  
}
